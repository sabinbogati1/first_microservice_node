const grpc = require("grpc");
const client_proto = require("./helpers/protoHelper");//protoLoader

     main=()=>{
        const client = new client_proto.Client('localhost:50055', grpc.credentials.createInsecure());

        client.sayHello({name: "sabin"}, (err, res) =>{
            console.log("Say Hello client --> ", res);
        });

        client.addClient({  "name": "jjj","email": "sabin@gmail.com","phoneNumber": "885563633"}, (err, res)=>{
            console.log(" add client response ", res);
        })

    }

    main();
