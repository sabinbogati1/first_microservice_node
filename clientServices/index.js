const connection = require("../config/databaseConfig");

sayHello = (call, callback) => {
   return callback(null, {message: "Hello World " + call.request.name})
}

addClient = (call, callback) => {
    console.log(" client service ::  add client", call.request);
    connection.query('INSERT INTO client SET ?', {name: call.request.name, email: call.request.email, phonenumber: call.request.phoneNumber}, (error, results, fields)=> {
        if (error){
            return callback(null, {message: 'Client Successfully added '});
        }
       return callback(null, {message: 'Client Successfully added '});
      });
  };


getAllClient =(call, callback) => {
    connection.query('SELECT * FROM client', (error, results, fields)=> {
        if (error){
           return callback(null, {message: `Error while getting All client `})
        } 
        console.log(" results :: ", results);
       return callback(null, {message: 'Get All Client Success '});
      });
}

module.exports = { 
    addClient,
    getAllClient,
    sayHello
};