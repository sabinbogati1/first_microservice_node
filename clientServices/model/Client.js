const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const ClientSchema = new Schema({ 
        name:{
            type: String
        },
        email:{
            type: String
        },
        phoneNumber:{
            type: String
        },
        date: {
            type: Date,
            default: Date.now
        }
})

module.exports = Post = mongoose.model("post", ClientSchema);