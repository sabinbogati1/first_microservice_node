const PROTO_PATH = __dirname + "/../protos/client.proto";
const protoLoader = require('@grpc/proto-loader');
const grpc = require("grpc");
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });

const client_proto = grpc.loadPackageDefinition(packageDefinition).bitsbeat;

module.exports = client_proto;
