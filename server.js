const grpc = require("grpc");

const clientServer = require("./clientServices");

const client_proto = require("./helpers/protoHelper");//protoloader


main = () =>{
    const server = new grpc.Server();
    server.addService(client_proto.Client.service, {
        sayHello: clientServer.sayHello,
        addClient: clientServer.addClient,
        getAllClient: clientServer.getAllClient
    });
    server.bind('0.0.0.0:50055', grpc.ServerCredentials.createInsecure());
    server.start();
}

main();